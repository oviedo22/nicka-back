from rest_framework import viewsets

from .models import City, FlightClass, UsersBooking

from .serializers import CityModelSerializer, FlightClassModelSerializer, UsersBookingClassModelSerializer


# Create your views here.


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CityModelSerializer

class FlightClassViewSet(viewsets.ModelViewSet):
    queryset = FlightClass.objects.all()
    serializer_class = FlightClassModelSerializer

class UsersBookingClassViewSet(viewsets.ModelViewSet):
    queryset = UsersBooking.objects.all()
    serializer_class = UsersBookingClassModelSerializer
