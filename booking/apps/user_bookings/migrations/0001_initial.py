# Generated by Django 3.2.12 on 2022-03-11 00:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='FlightClass',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='UsersBooking',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('flight_type', models.CharField(max_length=200)),
                ('departure_date', models.DateField()),
                ('departure_time', models.TimeField()),
                ('passengers_count', models.IntegerField()),
                ('arriving_city', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='arriving_city', to='user_bookings.city')),
                ('departure_city', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='departure_city', to='user_bookings.city')),
                ('flight_class', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='flight_class', to='user_bookings.flightclass')),
            ],
        ),
    ]
