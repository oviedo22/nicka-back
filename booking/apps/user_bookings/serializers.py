from rest_framework import serializers

from .models import City, FlightClass, UsersBooking


class CityModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class FlightClassModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlightClass
        fields = '__all__'


class UsersBookingClassModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = UsersBooking
        fields = '__all__'

    def validate(self, data):
        flight_type = data.get('flight_type')
        arriving_city = data.get('arriving_city')
        passengers_count = data.get('passengers_count')

        if passengers_count < 1:
            raise serializers.ValidationError(
                'Cantidad de pasajeros no puede ser menor o igual a 0')

        if flight_type == 'Ida' and arriving_city != None:

            data['arriving_city'] = None

        return data