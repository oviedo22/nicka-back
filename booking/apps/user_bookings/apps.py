from django.apps import AppConfig


class UserBookingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.user_bookings'
