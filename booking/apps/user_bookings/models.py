from django.db import models

# Create your models here.

class City(models.Model):
    name= models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name}'

class FlightClass(models.Model):
    name= models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name}' 

class UsersBooking(models.Model):
    
    flight_type = models.CharField(max_length=200)
    departure_city = models.ForeignKey(City, related_name='departure_city', on_delete=models.PROTECT)
    arriving_city = models.ForeignKey(City, related_name='arriving_city', on_delete=models.PROTECT, null=True, blank=True)
    departure_date=models.DateField()
    departure_time=models.TimeField()
    passengers_count=models.IntegerField()
    flight_class = models.ForeignKey(FlightClass, related_name='flight_class', on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.flight_type} - {self.departure_city} - {self.arriving_city} - {self.passengers_count}' 