FROM python:3.9.6

WORKDIR /code/

COPY requirements.txt /code/

RUN pip install pipenv

RUN pip install -r requirements.txt

ENV PYTHONIOENCODING="utf-8"

EXPOSE 8000